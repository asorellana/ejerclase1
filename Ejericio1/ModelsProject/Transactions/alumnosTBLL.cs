﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelsProject.Transactions
{
    public class alumnosTBLL
    {

        private dbEscolarEntities context;
        public alumnos alumno { get; set; }




        public alumnosTBLL(bool withContext)
        {
            if (withContext)
            {
                context = new dbEscolarEntities();
            }
        }

        public void Get(int id)
        {
            alumno = context.alumnos.Find(id);
        }

        public void create(alumnos a)
        {
            using (dbEscolarEntities db = new dbEscolarEntities())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        db.alumnos.Add(a);
                        db.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
        }

        public void update()
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    context.Entry(alumno).State = System.Data.Entity.EntityState.Modified;
                    context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        public void delete()
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    context.Entry(alumno).State = System.Data.Entity.EntityState.Deleted;
                    context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }
    }
}
