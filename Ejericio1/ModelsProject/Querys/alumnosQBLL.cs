﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelsProject.Querys
{
    public static class alumnosQBLL
    {
        public static List<alumnos> GetAlumnos()
        {
            dbEscolarEntities db = new dbEscolarEntities();
            return db.alumnos.ToList();
        }

        public static List<alumnos> GetAlumnos(string apellido)
        {
            dbEscolarEntities db = new dbEscolarEntities();
            return db.alumnos.ToList();
        }

        public static alumnos GetAlumno(int id)
        {
            dbEscolarEntities db = new dbEscolarEntities();
            return db.alumnos.FirstOrDefault(x => x.id == id);
        }
        
        public static alumnos GetAlumno(string cedula)
        {
            dbEscolarEntities db = new dbEscolarEntities();
            return db.alumnos.FirstOrDefault(x => x.cedula == cedula);
        }

       
    }
}
