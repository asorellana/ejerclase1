﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ModelsProject
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class dbEscolarEntities : DbContext
    {
        public dbEscolarEntities()
            : base("name=dbEscolarEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<alumnos> alumnos { get; set; }
        public virtual DbSet<area> area { get; set; }
        public virtual DbSet<calificaciones> calificaciones { get; set; }
        public virtual DbSet<materias> materias { get; set; }
        public virtual DbSet<matriculas> matriculas { get; set; }
    }
}
